import copy
import itertools
import sage.rings
import sage.all

# Given k, n and the homogeneous coordinate ring of the ambient projective
# space, return a dictionary of Plucker coordinates for the
# Grassmannian Gr(k,n): the keys are increasing sequences of length
# k in {1, ..., n}, interpreted as vertical steps of a Young diagram in the
# k x (n-k) grid; ordering them by lex, the i-th has value a SageMath
# symbolic variable named 'xi'

def variables(k, n, proj_ring):
	
	p = {}
	
	i = 0
	gen_list = proj_ring.gens()
	for S in itertools.combinations(range(1,n+1), k):
		p[tuple(sorted(tuple(S)))] = gen_list[i]
		i = i + 1
					
	return p

# Return +/- 1 according to the sign of permutation that sorts the input
# list in increasing order, without modifying it

def signsort(l):
	
	inversions = 0
	for i in range(len(l)):
		j = i + 1
		while (i < j) and (j < len(l)):
			if l[i] > l[j]:
				inversions = inversions + 1
			j = j + 1
	
	if (inversions % 2) == 0:
		return 1
	else:
		return -1


# Give k, n and the list of Plucker variables correspoding to Young diagrams
# in the k x (n-k) grid, generate a list whose elements are SageMath symbolic
# expressions encoding the Plucker relations for the Grassmannian Gr(k,n)
# according to Fulton.
#
# [TODO] Make this faster. This is currently O(2^k * n^{2k}), whereas the
# number of Plucker relations is O(1/(k!)^2 * n^{2k}) (see Speyer's argument
# on mathoverflow for estimate on number of Plucker relations).

def rel_list(k, n, p):

	relations = []
	
	for d1 in itertools.combinations(range(1,n+1), k):
		for d2 in itertools.combinations(range(1,n+1), k):
			for u in range(1,k+1):
			# Recall that itertools.combination returns tuples ordered in increasing order
				plucker_lhs = p[d1]*p[d2]
				plucker_rhs = 0
			 	for l in itertools.combinations(range(1,k+1), u):
			 		monomial = 0
			 		l = sorted(list(l))
			 		temp1 = []
			 		for i in l:
			 			temp1.append(d1[i-1])
			 		temp2 = list(d2[k-u:])
			 		d1_swapped = list(copy.copy(d1))
			 		d2_swapped = list(copy.copy(d2))
			 		for i in l:
			 			d1_swapped[i-1] = temp2.pop(0)
			 		for i in range(k-u,k):
			 			d2_swapped[i] = temp1.pop(0)
			 		sign1 = 0
			 		if len(d1_swapped) == len(set(d1_swapped)):
			 			sign1 = signsort(d1_swapped)
			 		sign2 = 0
			 		if len(d2_swapped) == len(set(d2_swapped)):
			 			sign2 = signsort(d2_swapped)
			 		if sign1*sign2 != 0:
			 			plucker_rhs = plucker_rhs + sign1*sign2*p[tuple(sorted(d1_swapped))]*p[tuple(sorted(d2_swapped))]
				if plucker_lhs != plucker_rhs:
					relations.append(plucker_lhs - plucker_rhs)
		
	return relations
