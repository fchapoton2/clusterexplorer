# Given k and n, generate all Young diagrams in k x (n-k) grid centered at
# top left corner, encoded in a list of k-tuples (d_1, ..., d_k) where d_i
# is the number of boxes in row i

def list_all(k, n):

	l = []
	diagrams = []
	
	if (k == 1):
		for a in range(n):
			l.append([a])
		return l
	
	for a in range(n-k+1):
		l = list_all(k-1, a+k-1)
		for i in range(len(l)):
			l[i] = [a] + l[i]
		diagrams = diagrams + l
	
	return diagrams


# Given a Young diagram in a k x (n-k) grid in terms of the list of its
# k vertical steps, return its partition form (d_1, ..., d_k)

def vstep_to_part(k, n, ver_steps):
		
	d = []
	
	d.append(n-k-ver_steps[0]+1)
	for i in range(1, k):
		d.append(d[i-1] - (ver_steps[i]-ver_steps[i-1]) + 1)
	
	return d
	

# Given a in [0, k] and b in [0, n-k], return the vertical steps of the
# a x b rectangular Young diagram in the k x (n-k) grid

def vstep_of_rect(a, b, k, n):

	v_steps = []
	
	if a == 0 or b == 0:
		for i in range(n-k+1, n+1):
			v_steps.append(i)
		return v_steps
	
	for i in range(n-k-b+1, n-k-b+a+1):
		v_steps.append(i)
	for i in range(n-k+a+1, n+1):
		v_steps.append(i)
		
	return v_steps
	

# Given a Young diagram d in the k x (n-k) grid represented as partition,
# determine if it's a boundary rectangular diagram or not
	
def is_frozen(k, n, d):
	
	# Check if d is the empty diagram
	
	if d[0] == 0:
		return True
		
	# Check if d is a rectangle
	
	for i in range(len(d)):
		if d[i] != d[0] and d[i] > 0:
			return False
				
	# Check if d is a full width rectangle
	
	if d[0] == n-k:
		return True
		
	# Check if d is a full height rectangle
	
	if d[k-1] > 0:
		return True
	
	
	return False
