import itertools
import math

import sage.all

# Given k, n and a Laurent polynomial f in the Plucker variables of a plabic
# cluster C of type (k,n), return the Newton polytope of f.

def newton_polytope(k, n, f, C):

	N = math.factorial(n) / ( math.factorial(k)*math.factorial(n-k) )
	# Compute exponent vectors of Laurent polynomial f, and then remove for
	# each the entries not corresponding to Plucker coordinates not labelled
	# by Young diagrams in C and the one corresponding to the empty diagram
	
	exponents = f.exponents()
	mask = [0,]*N
	i = 0
	for S in itertools.combinations(range(1,n+1), k):
		if tuple(sorted(tuple(S))) in C:
			mask[i] = 1
		i = i + 1
	mask[N-1] = 0
	for i in range(len(exponents)):
		exponents[i] = list(itertools.compress(exponents[i], mask))

	P = sage.all.Polyhedron(exponents)
	
	return P
	
# Given a Newton polytope P, test if it's a terminal lattice polytope
# or not, i.e. if the vertices are the only lattice points besides the
# origin.

def is_terminal(P):

	if P.integral_points_count() == P.n_vertices()+1:
		return True
	else:
		return False
		
# Given a Newton polytope P, test if the naive simplicial refinement
# on smae rays of the face fan is automatically smooth.
		
def is_small(P):

	fan_P = sage.all.FaceFan(P)
	fan_P = fan_P.make_simplicial()
	if fan_P.is_smooth():
		return True
	else:
		return False


	
# Given a list of ray generators, return True if the corresponding cone
# has a small simplicial resolution and False otherwise
	
def small_res_cone(rays_list):

	main_cone = sage.all.Cone(rays_list)
	d = len(rays_list)
	main_cone_dim = main_cone.dim()
		
	# Construct a list of all possible sets of rays of simplicial cones
	# contained in main_cone with no new rays
	
	T_list = []
	for T in itertools.combinations(range(d), main_cone_dim):
		T_list.append(T)
	
	# For each possible collection of sets above, verify if it gives
	# rise to a small resolution of main_cone
	
	for triang_size in range(1,len(T_list)+1):
	
		for triang in itertools.combinations(T_list, triang_size):
			rays_used = [0,]*d
			cones_triang = []
			discard_triang = False
			for T in triang:
				rays_T = []
				for i in range(len(rays_list)):
					if i in T:
						rays_T.append(rays_list[i])
						rays_used[i] = 1
				cone_T = Cone(rays_T)
				if cone_T.is_smooth():
					cones_triang.append(Cone(rays_T))
				else:
					discard_triang = True
					break
			if discard_triang == False:
				#print('Candidate triangulation = {}'.format(triang))
				
				# Check covering condition
				covering_check = True
				for flag in rays_used:
					if flag == 0:
						covering_check = False
						break
						
				if covering_check:
					
					# Check intersection condition
					intersection_check = True
					try:
						F = sage.all.Fan(cones_triang)
					except ValueError:
						intersection_check = False
			
				# Check if the resulting simplicial refinement of main_cone
				# is actually smooth
				if covering_check and intersection_check:
					#print('Cones over triangulation give a small simplicial resolution!')
					return True

	return False
