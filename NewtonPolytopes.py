import copy
import itertools
import math
import random

import sage.all
import sage.graphs.all
import sage.graphs.digraph
import sage.combinat.sf.classical
import sage.geometry.lattice_polytope
import sage.geometry.fan
import sage.interfaces.latte
import sage.rings
import sage.structure.factory

import newton
import plucker
import potentials
import walk

# Take in input k and n

print('Input k: ')
k = input()
print('Input n: ')
n = input()
print('Type 0 for exhaustive mode or 1 for random mode: ')
newton_mode = input()
if newton_mode == 1:
	print('Type size of the sample as % of the total: ')
	newton_percentage = input()
print('Compute f-vectors?')
show_fvector = input()
print('Check reflexivity?')
show_reflexive = input()
print('Check terminality?')
show_terminal = input()
print('Check existence of small resolution?')
show_small = input()
print('Compute volume?')
show_volume = input()

# Compute N = {n choose n-k}, total number of critical points of mirror
# Landau-Ginzburg superpotential on Gr(n-k, n)

N = math.factorial(n) / ( math.factorial(k)*math.factorial(n-k) )
print('Total number of critical points: {}'.format(N))

# Construct the homogeneous coordinate ring of the ambient
# projective space for the Plucker embedding, and inject its generators
# in the variables namespace

proj_ring = sage.rings.all.PolynomialRing(sage.all.QQ, 'x', N)
proj_ring.inject_variables(verbose=False)
	
# Define the Plucker coordinates corresponding to the generators
# of the homogeneous coordinate ring of the ambient projective space
	
p = plucker.variables(n-k, n, proj_ring)
	
# Construct the Plucker relations describing the homogeneous coordinate ring
# of Gr(n-k,n) in the Plucker embedding
	
relations = plucker.rel_list(n-k, n, p)

# Compute the local Landau-Ginzburg potentials of the mirror to Gr(k,n)

print('About to start random walk')
W = walk.random_walk(n-k, n, p, relations, loc_potentials=True)
print('Random walk is over')

# Compute Newton polytopes and visualize the desired combinatorial
# informations for all clusters or MAX_NEWTON random clusters
# depending on whether user specified exhaustive or random mode.

if newton_mode == 0:
	for C in W.keys():
		print('Cluster: {}'.format(C))
		P = newton.newton_polytope(n-k, n, potentials.force_laurent(W[tuple(C)], sage.all.LaurentPolynomialRing(sage.all.QQ, 'x', N)), C)
		if show_fvector == 1:
			print('f-vector = {}'.format(P.f_vector()))
		if show_reflexive == 1:
			print('reflexive = {}'.format(P.is_reflexive()))
		if show_terminal == 1:
			print('terminal = {}'.format(newton.is_terminal(P)))
		if show_small == 1:
			print('small resolution: {}'.format(newton.is_small(P)))
		if show_volume == 1:
			print('volume: {}'.format(P.volume()))
else:
	sample_size = int( math.ceil( float(newton_percentage)/float(100) * len(W) ) )
	for C in random.sample(list(W.keys()), sample_size):
		print('Cluster: {}'.format(C))
		P = newton.newton_polytope(n-k, n, potentials.force_laurent(W[tuple(C)], sage.all.LaurentPolynomialRing(sage.all.QQ, 'x', N)), C)
		if show_fvector == 1:
			print('f-vector = {}'.format(P.f_vector()))
		if show_reflexive == 1:
			print('reflexive = {}'.format(P.is_reflexive()))
		if show_terminal == 1:
			print('terminal = {}'.format(newton.is_terminal(P)))
		if show_small == 1:
			print('small resolution: {}'.format(newton.is_small(P)))
		if show_volume == 1:
			print('volume: {}'.format(P.volume()))
		
			

